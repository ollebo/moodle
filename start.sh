#!/bin/bash
#
#
#

#echo $SITENAME

if [ -n "$SITENAME" ]
then

  if [ "$PROTO" == "http" ]  && [ -n "$PROTO" ];
  then


  	echo "Setting sitename No TLS for dev"
    envsubst '$DBHOST $DB $DBUSER $DBPASS $PROTO $SITENAME $REDIS'< /config_no_tls.tpl > /var/www/html/moodle/config.php


    #Make dir if not exsist
  	mkdir -p /var/www/html/shared/moodledata
  	mkdir -p /var/www/html/shared/temp
  	mkdir -p /var/www/html/shared/cache
  	mkdir -p /var/www/html/local/cache
    mkdir -p /var/www/html/moodle/healthz
    echo  "$SITENAME"  >> /var/www/html/moodle/healthz/index.html	
	  chmod 777 -R /var/www/html/shared/moodledata
    #chown www-data:www-data -R /var/www/html/
    chmod 777 -R /var/www/html/shared/

  else

    echo "Setting sitename"
    envsubst '$DBHOST $DB $DBUSER $DBPASS $PROTO $SITENAME $REDIS'< /config.tpl > /var/www/html/moodle/config.php


    #Make dir if not exsist
    mkdir -p /var/www/html/shared/moodledata
    mkdir -p /var/www/html/shared/temp
    mkdir -p /var/www/html/shared/cache
    mkdir -p /var/www/html/local/cache
    mkdir -p /var/www/html/moodle/healthz
    echo  "$SITENAME"  >> /var/www/html/moodle/healthz/index.html 
    chmod 777 -R /var/www/html/shared/moodledata
    #chown www-data:www-data -R /var/www/html/
    chmod 777 -R /var/www/html/shared/
   fi 


else
   echo "No sitename use localhost"

fi


php-fpm7.2
nginx -g 'daemon off;'
