# Moodle Developer base 


This is the dev version for moodle to use inside uggla/ollebo/elino hosting.
As a developer start up the stack and start working


- Updated to moodle 3.8



## Pre requerment


- docker
- docker-compose



**First install docker and docker-compose**

Start the setup with 

```
docker-compose up
```


This will bring up the developer docker images and setup the wordpress for you.
You can now wisit your wordpress on your local computer at


### For moodle

**http://localhost**


If you need a nother port modify the docker-compose.yaml and change port 80 to other port.
You can also change so that wordpress 5 is running on port 80 and 443

```
     ports:
       - "80:8000"
       - "443":8443

```

### Phpmyadmin

phpmyadmin is a database tool included and can be found on **http://localhost:82** from here you can performe database actions to the database.


### When creating barking changes to moodle 

When we create braking changes to moodle we can create a new git repo and push this into that repo and then we can easy build the new version 
