<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = '$DBHOST';
$CFG->dbname    = '$DB';
$CFG->dbuser    = '$DBUSER';
$CFG->dbpass    = '$DBPASS';
$CFG->prefix    = 'prod_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '',
);






#Cluster
$CFG->wwwroot   = '$PROTO://$SITENAME';
$CFG->dataroot  = '/var/www/html/shared/moodledata';

$CFG->dirroot   = '/var/www/html/moodle';
$CFG->tempdir   = '/var/www/html/shared/temp';
$CFG->cachedir  = '/var/www/html/shared/cache';
$CFG->localcachedir =  '/var/www/html/local/cache';



#redis
$CFG->session_handler_class = '\core\session\redis';
$CFG->session_redis_host = '$REDIS';
$CFG->session_redis_port = 6379;  // Optional.
$CFG->session_redis_database = 0;  // Optional, default is db 0.
$CFG->session_redis_prefix = ''; // Optional, default is don't set one.
$CFG->session_redis_acquire_lock_timeout = 120;
$CFG->session_redis_lock_expire = 7200; 7200;       // Ignored if memcached extension <= 2.1.0

$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
